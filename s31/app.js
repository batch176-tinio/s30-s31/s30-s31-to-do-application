// Mini-activity - Setup basic JS server

// Dependencies:
	const express = require("express");
	const mongoose = require("mongoose");
	const taskRoute = require("./routes/taskRoute");

// Server setup
	const app = express();
	const port = 4000;

// Middleware
	app.use(express.json());
	app.use(express.urlencoded({extended: true})); // if request is coming from form. it can be excluded

// Database Connection[void]
	// mongoose.connect("mongodb+srv://seph99:admin123@cluster0.u6eeb.mongodb.net/toDo176?retryWrites=true&w=majority", {

	// the following codes no longer needed to be added since nodejs version is updated
	// if nodejs is not updated, there will be deprecation warning
	// 	useNewUrlParser: true,
	// 	useUnifiedTopology: true
	// });

// Database Connection
	mongoose.connect("mongodb+srv://seph99:admin123@cluster0.u6eeb.mongodb.net/toDo176?retryWrites=true&w=majority");

	let db = mongoose.connection;
	db.on('error', console.error.bind(console,'Connection Error'));
	db.once('open', () => console.log('Connected to Mongodb'));


// Add task route
	// if get("/", ...) in taskRoute.js, endpoint in postman is /tasks/ or /tasks
	// if get("/xxx", ...) in taskRoute.js, endpoint in postman is /tasks/xxx
	app.use("/tasks", taskRoute);


// Entry Point Response
	app.listen(port, () => console.log(`Server is running at port ${port}.`))









