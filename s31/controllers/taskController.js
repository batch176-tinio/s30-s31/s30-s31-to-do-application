// Contollers contain the functions and business logic of our expressjs app

// Import/require the model
const Task = require("../models/task");

//[Controllers]

//[SECTION] - Retrieve
	module.exports.getAllTasks = () => {

		return Task.find({}).then(result => {
			return result;
		});

	};

// temp
	// module.exports.createTask = (requestBody) => {

	// 	let newTask = new Task({
	// 		name: requestBody.name
	// 	});
// temp
	// return newTask.save()
	// .then((task, error) => {
	// 	if(error) {
	// 		return false
	// 	} else {
	// 		return task
	// 	}

	// })

//[SECTION] - Create
	//The request body coming from the client was passed from the "taskRoute.js" file via the "req.body" as an argument and is renamed as a "requestBody" parameter in the controller file
	module.exports.createTask = (requestBody) => {


		//Creates a task object based on the Mongoose model "Task"
		let newTask = new Task({

			//Sets the "name" property with the value received from the client/Postman
			name: requestBody.name
		})


	// Saves the newly created "newTask" object in the MongoDB database
	// The "then" method waits until the task is stored in the database or an error is encountered before returning a "true" or "false" value back to the client/Postman
	// The "then" method will accept the following 2 arguments:
		// The first parameter will store the result returned by the Mongoose "save" method
		// The second parameter will store the "error" object
	// Compared to using a callback function on Mongoose methods discussed in the previous session, the first parameter stores the result and the error is stored in the second parameter which is the other way around
		// Ex.
			// newUser.save((saveErr, savedTask) => {})
		return newTask.save().then((task, error) => {

			// If an error is encountered returns a "false" boolean back to the client/Postman
			if(error) {
				return false
			// Save successful, returns the new task object back to the client/Postman
			} else {
				return task
			}
		})
		
	// using then-catch
		// return newTask.save()
		// .then(result => {
		// 	return result
		// })
		// .catch(error => {
		// 	return error
		// });

	}

//[SECTION] - Update
	// 1. Change status of task. from pending to completed
	// we will reference the document using its ID field.

	module.exports.taskCompleted = (taskId) => {	
		// Query or search for the desired task to update.
		// the findById() mongoose method will look for a resource (task) which matches the ID from the URL of the request.
		// Upon performing this method inside our collection, a new promise will be instantiated, so we need to able to handle the possible outcome of queries.
		return Task.findById(taskId).then((found,error) => {
			// describe how we're going to handle of the promise using a selection control structure.
			// process the document found from the collection and change the status from pending to completed.

			if(found) {
				// call out the parameter that describes the result of the query when successful.
				console.log(found); // document found the db should be displayed in the terminal
				// Modified the status of returned document to completed.
				found.status = 'Completed';
				// Save the new changes inside our database
				// upon saving the new changes for this document, a 2nd promise will be instantiated.
				// We will chain a 'thenable' expression upon performing a save() method into our returned document.
				return found.save().then((updatedTask,saveErr) => {
					// catch the state of the promise to identify a specific response
					if (updatedTask) {
						return 'Task has been successfully modified';
					} else {
						return 'Task failed to update'
					};
				});
				// return 'Match found for Task.'
			} else {
				return 'No match found.';
			}


		// then-catch also does not work for some reason.
		// return Task.findById(taskId)
		// .then( () => {
		// 	return 'Match found'
		// })
		// .catch( () => {
		// 	return 'Error! No match found'
		// });
			

			
		// for some reason the following code does not work
		// 	if(error) {
		// 		return 'Error! No match found'
		// 	} else {
		// 		console.log(found);
		// 		return 'Match found.';
		// 	}

		});


	};	

	// 2. Change status of task from completed to pending
	module.exports.taskPending = (userInput) => {
		// expose this new component
		// search database for the user Input.
		// findById mongoose method -> will run search query inside our database using id field as reference.
		// since performing this method will have 2 possible states/outcome, we will chain a then expression to handle the states of the promise.
		return Task.findById(userInput).then((result,err) => {
			// handle and catch the state of promise.
			// assign and invoke in route module.
			if (result) {
				// process the result of query and extract the property to modify its value.
				result.status = 'Pending' 
				return result.save().then((taskUpdated, error) => {
					// create a control structure to identify the proper response if the update was successfully executed.
					if (taskUpdated) {
						return 	`Task ${taskUpdated.name} was updated to Pending`
					} else {
						return 'Error when saving task updates'
					};

				});
				// return 'Match found';
			} else {
				return 'Something went wrong';
			};
		});
	};



//[SECTION] - Destroy
	// 1 Remove an existing resource inside the TASK collection.
		// expose the data across other modules in other app so that it will become reusable.
		// would we need input from the user? yes to identify which resource to target
	module.exports.deleteTask = (taskId) => {
		// how will the data will be processed in order to execute the task.
		// select which mongoose method will be used in order to acquire the desired end goal.
		// Mongoose provides an interface and methods to be able to manipulate the resources found inside the mongodb atlas.
		// in order to identify the location in which the function will be executed, append the model name (Task).

		// findbyIdandRemove => mongoose method which targets a document using its id field and removes the targeted document from the collection.
		// upon executing this method within our collection a new promise will be instantiated upon waiting for the task to be executed, however we need to be able to handle the outcome of the promise whatever state it may fall on.
		// States of promises in JS
			// => Pending (waiting to be executed)
			// => Fulfilled (successfully executed)
			// => Rejected (unfulfilled promise)
		// to be able to handle the possible states upon executing the task
		// we are going to insert a 'thenable' expression to determine HOW we will respond depending on the result of promise
		// identify the 2 possible states of promise using a then expression
		return Task.findByIdAndRemove(taskId).then((fulfilled,rejected) => {
			// identify how you will act according to the outcome
			if (fulfilled) {
				return 'The Task has been successfully removed';	
			} else {
				return 'Failed to remove Task';
			};
			// assign a new endpoint for this route

		});
	};









