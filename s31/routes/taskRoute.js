// Contains all the endpoints for our application
// We separate the routes such that app.js only contains information on the server.


const express = require("express");

// Create a Router instance that functions as a middleware and routing system. 
// This allows access to HTTP method middlewares that makes it easier to create routes for our application.
const router = express.Router();


// Require task controller
const taskController = require("../controllers/taskController");

// [Routes]
// Route for getting all the tasks

router.get("/",(req,res) => {
	taskController.getAllTasks()
	.then(resultFromController => res.send(resultFromController))
});

// Route for creating a task
router.post("/", (req,res) => {
	taskController.createTask(req.body)
	.then(resultFromController => res.send(resultFromController))
});
	
// Route for deleting a task
	// call out the routing component (router) to register a brand new enpoint
	// When integrating a path variable within the URI, you are also changing the behavior of the path from STATIC to DYNAMIC
		// 2 Types of Endpoints
			// Static Route
				// -> unchanging, fixed, constant, steady
			// Dynamic Route (simply add colon (:) to endponint )
				// -> interchangeable, not fixed
router.delete("/:task",(req,res) => {
	// identify the task to be executed within this endpoint
	// call out the proper function for this route (deleteTask) and identify the source/providerof function (taskController in line12)

	// determine whether the value inside the path variable is transmitted to the server.
	console.log(req.params.task)
	// place the value of the path variable inside its own container.
	let taskId = req.params.task
	// res.send('Hello from Delete') //temp response to check is setup is correct
	// make sure to pass down the identity of the task using the proper reference (objectID), however this time we are going to include the info as a path variable (integrating parametrized routes)
	// Path variable - will allow us to insert data within the scope of the URL.
		// what is variable? -> container, storage of info 
	// When is a path variable useful? 
		// -> when inserting only a single piece of info.
		// -> this is also useful when passing down info to REST API method that does not include a BODY section like 'GET'.

	// upon executing this method, a promise will be initialized so we need to handle the result of the promise in our route module.
	taskController.deleteTask(taskId).then(resultFromController => res.send(resultFromController));

}) 


// Route for update task status (Pending to Completed)
	// Create dynamic endpoint for this brand new route.
router.put("/:task",(req,res) => {
	// check if you are able to acquire the variables inside the path variables.
	// console.log(req.params.task) // temp response

	let idNiTask = req.params.task //declared to simplify calling out of path variable key

	// identify the business logic behind this task inside the controller module
	// call the intended controller to execute the process. make sure to identify the provider/source of function.
	// after invoking the controller method, handle the outcome
	taskController.taskCompleted(idNiTask).then(outcome => res.send(outcome));


// Route for update task status (Completed to Pending)
	// This will be the counter procedure for previous task.
router.put("/:task/pending", (req,res) => {
	let id = req.params.task;
	console.log(id); //temp response

	// declare business logic in controller module
	taskController.taskPending(id).then(outcome => {
		res.send(outcome);
	});

});





});




// temp
	// router.post("/", (req, res) => {

	// 	//The "createTask" function needs the data from the request body, so we need to supply it to the function
	// 	//If information will be coming from the client side commonly from forms, the data can be accessed from the request "body" property 
	// 	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
	// })





// Use "module.exports" to export the router object to use in the "app.js"
module.exports = router;

