// Create the Schema, model

const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({

	name: String,
	status: {
		type: String,
		default: "pending"
	}
})

// module.exports is used to treat this as package
module.exports = mongoose.model("Task", taskSchema);



